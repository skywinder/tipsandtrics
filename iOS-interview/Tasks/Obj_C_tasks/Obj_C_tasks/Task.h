//
//  Task.h
//  Obj_C_tasks
//
//  Created by Petr Korolev on 15/02/16.
//  Copyright © 2016 Petr Korolev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Task : NSObject

- (void)passVarToFunction;

- (void)q2;

- (void)shallowCopy;

- (void)deepCopy;

- (void)boolCheck;
@end
