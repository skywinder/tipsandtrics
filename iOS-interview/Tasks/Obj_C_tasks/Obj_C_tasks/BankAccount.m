//
//  BankAccount.m
//  
//
//  Created by Petr Korolev on 15/02/16.
//
//

#import "BankAccount.h"

@implementation BankAccount

-(id) copyWithZone: (NSZone *) zone
{
    BankAccount *accountCopy = [[BankAccount allocWithZone: zone] init];
    
    [accountCopy setAccount: accountNumber andBalance: accountBalance];
    return accountCopy;
}

@end
