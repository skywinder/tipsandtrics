//
//  Task.m
//  Obj_C_tasks
//
//  Created by Petr Korolev on 15/02/16.
//  Copyright © 2016 Petr Korolev. All rights reserved.
//

#import "Task.h"
#import "BankAccount.h"

@implementation Task
- (void)passVarToFunction {
    NSString *s = @"foo";
    [self modif:s];
    NSLog(@"s = %@", s);
    return;
}
- (void)q2 {
    __block NSString *s1 = @"foo";
    NSString *s2 = @"bar";
    NSString*(^printString)(NSString*) = ^(NSString *str){
        NSLog(@"s1 = %@", s1);
        NSLog(@"s2 = %@", s2);
        NSLog(@"str = %@", str);
        s1 = @"norf";
        return str;
    };
    s1 = @"qux";
    s2 = @"xyz";
    NSString *s3 = printString(s2);
    NSLog(@"s1 = %@", s1);
    NSLog(@"s2 = %@", s2);
    NSLog(@"s3 = %@", s3);
}

- (void)shallowCopy {
    NSArray *myArray1;
    NSArray *myArray2;
    NSMutableString *tmpStr;
    NSMutableString *string1;
    NSMutableString *string2;
    NSMutableString *string3;

    string1 = [NSMutableString stringWithString: @"Red"];
    string2 = [NSMutableString stringWithString: @"Green"];
    string3 = [NSMutableString stringWithString: @"Blue"];

    myArray1 = [@[string1, string2, string3] mutableCopy];

    myArray2 = [myArray1 copy];
    tmpStr = myArray1[0];

    [tmpStr setString: @"Yellow"];

    NSLog (@"First element of myArray2 = %@", myArray2[0]);
}


- (void)deepCopy {
    NSArray *myArray1;
    NSArray *myArray2;
    NSMutableString *tmpStr;
    NSMutableString *string1;
    NSMutableString *string2;
    NSMutableString *string3;
    NSData *buffer;


    string1 = [NSMutableString stringWithString: @"Red"];
    string2 = [NSMutableString stringWithString: @"Green"];
    string3 = [NSMutableString stringWithString: @"Blue"];

    myArray1 = [@[string1, string2, string3] mutableCopy];

    buffer = [NSKeyedArchiver archivedDataWithRootObject: myArray1];
    myArray2 = [NSKeyedUnarchiver unarchiveObjectWithData: buffer];

    tmpStr = myArray1[0];

    [tmpStr setString: @"Yellow"];

    NSLog (@"First element of myArray1 = %@", myArray1[0]);
    NSLog (@"First element of myArray2 = %@", myArray2[0]);
}

- (void)modif:(NSString *)s {
    s = @"bar";
}


//Что выведется в консоль? Почему?
- (BOOL)objectsCount
{
    NSMutableArray *array = [NSMutableArray new];
    for (NSInteger i = 0; i < 1024; i++)
    {
        [array addObject:[NSNumber numberWithInt:i]];
    }
    NSUInteger count = array.count;
//    NSUInteger count = 1024;
//    https://developer.apple.com/library/ios/documentation/General/Conceptual/CocoaTouch64BitGuide/Major64-BitChanges/Major64-BitChanges.html
//    https://www.bignerdranch.com/blog/bools-sharp-corners/
    return count;

}

- (void)boolCheck
{
    if ([self objectsCount])
    {
        NSLog(@"has objects");
    }
    else
    {
        NSLog(@"no objects");
    }
}

//Выведется ли в дебагер «Hello world»? Почему?
- (BOOL)debugSync
{
    dispatch_sync(dispatch_get_main_queue(), ^{
        NSLog(@"Hello world");
    });
    return YES;
}

@end
