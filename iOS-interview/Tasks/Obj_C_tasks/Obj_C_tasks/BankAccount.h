//
//  BankAccount.h
//  
//
//  Created by Petr Korolev on 15/02/16.
//
//

#import <Foundation/Foundation.h>

@interface BankAccount: NSObject <NSCopying>
{
    double accountBalance;
    long accountNumber;
}
-(void) setAccount: (long) y andBalance: (double) x;
-(double) getAccountBalance;
-(long) getAccountNumber;
-(void) setAccountBalance: (double) x;
-(void) setAccountNumber: (long) y;
-(void) displayAccountInfo;
-(id) copyWithZone: (NSZone *) zone;
@end
