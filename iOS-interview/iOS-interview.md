# Собеседование. Вопросы по собеседованию iOS

Собеседование. Вопросы.


#iOS 

##Beginner:

1. Чем отличается `NSMutableDictionary` от `NSDictionary`?

	**Ответ:**
	- второй можно модифицироать
	- при попытке записать в первый мы упадем

1. Можно ли сделать так и что будет напечатано:

	```
	NSDictionary *dict1 = @{@"key1": @"value1", @"key2": @"value2"};
	dict1 = @{@"key3": @"value3", @"key3": @"value3"};
	NSLog(dict1);
	
	```


	**Ответ:**
	
	Да, так можно, т.к. мы не модифицируем память, а меняем ссылку
	Без арк - это утечка памяти.

1. Для чего служит `reuseIdentifier`?

	**Ответ:**
	
	В TableView для переиспользования ячеек. т.е. на более низком уровне, когда мы сколлим список вниз - для новых элементов не создается новый объект, а переиспользуется ячейка, вышедшая из области видимости
	соответсвенно что бы различать типы ячеек - нужно использовать разные `reuseIdentifier`
	
	1. Какие методы с этим словом знаешь?

			
		**Ответ:**
		
		- `initWithStyle:reuseIdentifier:` инициализация
		- `registerClass forCellReuseIdentifier` - регистрация нибника
		- `dequeueReusableCellWithIdentifier:` - найти ячеку для переиспользования
		- начиная с 5-й оси:
		`dequeueReusableCellWithIdentifier forIndexPath:` тоже, только всегда возвращает алоцированую ячейку


1. Расскажи про **делегирование, протоколы**.

	- Написать протокол
		- Объяснить, зачем там `NSObject`

	- Категории(category)

		- Когда лучше использовать категорию, а когда наследование?

			- **Ответ:**	если не нужно создавать новые переменные - то наследование


1. `NSNotificationCenter` для чего служит?

	- В каких случаях следует использовать делегаты, а в каких `NSNotification`?

	**Ответ:** Когда не знаем, кто точно должен регагировать на данное действие, или будет несколько “ответчиков”  - нотификация. В остальных делегаты.

1. Какие сторонние либы знакомы? с какими имел дело?

1. Жизненный цикл `UIViewController`’a http://habrahabr.ru/post/129557/

1. Что такое коллекции? Какие типы коллекций ты знаешь? Какие и когда нужно использовать?
	1. `NSArray` vs `NSSet` - что лучше и когда?
		- NSArray is faster than NSSet for holding and iterating. As 50% faster for constructing and as much as 500% faster for iterating. 
	**Lesson: if you only need to iterate contents, don't use an NSSet.**

		- If you need to test for inclusion, work hard to **avoid NSArray**. Even if you need both iteration and inclusion testing, you should probably still choose an NSSet. If you **need to keep your collection ordered and also test for inclusion**, then you should **consider keeping two collections** (an NSArray and an NSSet), each containing the same objects.

1. `NSNull` и `nil` - в чем отличие? (пример кода с невалидным нилом в коллекции)


### Memory (Работа с памятью)

1. Какие типичные ошибки пр работе с памятью ты знаешь?

	**Memory leaks appears mostly due the following 4 issues:**
	
	- Not invalidating NSTimers when dismissing view controllers
	- Forgetting to remove any observers to NSNotificationCenter when dismissing the view controller.
	- Keeping strong references to self in blocks.
	- Using strong references to delegates in view controller properties

	Luckily I came across the following blog post and was able to correct them: http://www.reigndesign.com/blog/debugging-retain-cycles-in-objective-c-four-likely-culprits/


### Advanced:

1. Какие конструторы есть у VC? Какие и когда вызываются?

1. Чем отличается вызов переменной класса `self.iVar` от `_iVar`
Ответ: Через _ мы получаем доступ по адресу, минуя сеттер и геттер

1. Написать реализацию синглтона.
Ответ:
http://www.galloway.me.uk/tutorials/singleton-classes/

	```
	+ (id)sharedManager {
	static MyManager *sharedMyManager = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedMyManager = [[self alloc] init]; });
	return sharedMyManager;
	}

	- (id)init {
	if (self = [super init]) {
			someProperty = [[NSString alloc] initWithString:@"Default Property Value"];
		}
		return self;
	}
	```
	
	Swift:
	
	```
	class MySingletonClass {
   		 static let sharedInstance = MySingletonClass()
		 private init() {
		    }
	}
	```
	

1. Как работает `NSRunloop`?

1. `Autoreleasepool` + @autorelease

1. Есть ли Garbage Collector в iOS, MacOs?
	Ответ:
	В iOS нет, тут только ручной или автоматический подсчет ссылкок
	В МакОси - есть.

1. What is the difference between Objective-C automatic reference counting and garbage collection?
http://stackoverflow.com/questions/7874342/what-is-the-difference-between-objective-c-automatic-reference-counting-and-garb

	>ARC inserts the appropriate retains and releases required for reference counting at compile time, by applying the rules that all Objective-C developers have had to use over the years. This frees the developer from having to manage this themselves. Because the retains and release are inserted at compile time, no collector process is needed to continually sweep memory and remove unreferenced objects.

	>One slight advantage that tracing garbage collection has over ARC is that ARC will not deal with retain cycles for you, where tracing garbage collection can pick these up.


###Многопоточность / Threading

1. Доступ к одному объекту из разных потоков.
1. Асинхронное выполнение, потом другой процесс после выполнения. 

1. Какие операции следует выполнять в main thread, какие нет?

	Ответ:
	- Вся графическая часть обязательна должна быть в мейнтреде.
	- Посомтреть на ход мыслей про многопоточность.

1. Можно ли посылать Notifications из неосновного треда и что тогда произойдет?
	Ответ:
	- Можно, тогда все, кто поймают нотификацию будут исполнять ее в том же потоке, а не главном.

	
1. Что выведет код, если его запустить из главного потока?


		dispatch_sync(dispatch_get_main_queue(), ^(void) {
		                NSLog(@"on main thread sync!");
		            });

		 NSLog(@“on main thread after!");


	**Ответ**: ничего. программа повиснет


1. Difference between objectForKey and valueForKey?
	http://stackoverflow.com/questions/1062183/difference-between-objectforkey-and-valueforkey
	- objectForKey for NSDictionary and NSArray, valueForKey is KVO.
	- objectForKey can accept any NSObject as key, valueForKey only NSString.

1. When should I call super? http://stackoverflow.com/questions/3906704/when-should-i-call-super

	>The usual rule of thumb is that when you are overriding a method that does some kind of initialization, you call super first and then do your stuff. And when you override some kind of teardown method, you call super last:

	```
	- (void) setupSomething {
	    [super setupSomething];
	    …
	}

	- (void) tearDownSomething {
	    …
	    [super tearDownSomething];
	}
	```


	>The first kind are methods like init…, viewWillAppear, viewDidLoad or setUp. The second are things like dealloc, viewDidUnload, viewWillDisappear or tearDown. This is no hard rule, it just follows from the things the methods do.
	

##Obj-C

1. В чем отличие `[self method]` и `[self performSelector:@selector(method)]`?

	**Ответ:** Это оно и тоже. Но во втором случае не будет проверти, существует ли такой метод и компилятор не ругнется на ошибку.

	For example, all three of the following messages do the same thing:

	```
	id myClone = [anObject playButtonSound];
	id myClone = [anObject performSelector:@selector(playButtonSound )];
	id myClone = [anObject performSelector:sel_getUid("playButtonSound")];

	//Here compiler will check if your object responds to -playButtonSound message and will give you a warning if it does not.
	[self playButtonSound];


	[self performSelector:@selector(playButtonSound)]; - Calling -playButtonSound this way you will not get compiler warning. However you can check dynamically if objects responds to a given selector - so you can safely attempt to call arbitrary selector on a object without specifying its type and not getting compiler warnings (that may be useful for example for calling optional methods in a objects delegate):



		if ([self respondsToSelector:@selector(playButtonSound)])

		  [self performSelector:@selector(playButtonSound)];

	```


1. What's the difference between a method and a selector?  (+ and what is Message?)
	> Selector is the name of a method.

	> Message - a message is a selector and the arguments you are sending with it.

	> Method - a method is a combination of a selector and an implementation

1. Категории. Зачем нужны? Почему не наследоватся?

1. Можно ли переопределить метод в категории?

	Ответ: можно, но ни при каких обстоятельствах не нужно это делать
	т.к. если переопределить метод в 2-х категориях - не известно какой вызовется. (надо наследоваться)




1. Core Graphics memory mangment.

1. Как связаны `NSRunLoop` и `NSAutoreleasePool` на пальцах?

1. Для чего используется протокол NSCopying?
	- почему мы не можем просто использовать любой собственный объект в качестве ключа в словарях (NSDictionary) , что нужно сделать чтобы решить эту проблему? 
	- разница между глубоким и поверхностным копированием

	- Deep copy, Shallow copy, differences, how to implement. 


###Other:

1. What’s the main problem with ARC?
2. When are the objects deallocated?
2. Tell me about the MVC pattern.
2. Tell me about MVVM.
3. Tell me about an architecture you implemented and that you are proud of
4. Is a block an object?



---

###Swift

1. What’s the swift language feature you most appreciated so far?
2. Unwrapping optional variables
3. difference between a Swift method and function:
> Methods are functions that are associated with a particular type. Classes, structures, and enumerations can all define instance methods, which encapsulate specific tasks and functionality for working with an instance of a given type. Classes, structures, and enumerations can also define type methods, which are associated with the type itself. Type methods are similar to class methods in Objective-C.
TL;DR: Functions are standalone, while methods are functions that are encapsulated in a class, struct, or enum.
http://www.objc.io/issue-16/swift-functions.html

- `Class` vs `Struct`
- `var` vs `let`
	- memory allocation

###Blacklane questions:

1. отличие `NSArray vs NSSet vs NSDictionary`
	- скорость доступа и итераций

	- **NSDictionary**

		- access time for a value in a `CFDictionary` object is guaranteed to be at worst `O(log N)` for any implementation, but is often O(1) (constant time)
		- Insertion or deletion operations - **constant time** as well, but are `O(N*log N)` in the worst cases.
		- `Dictionaries` tend to use significantly **more memory** than an `array` with the same number of values.
	- **NSArray**
		- The access at worst O(lg N), but often O(1)
		- Linear search operations, Insertion or deletion similarly have a worst O(N*lg N)


---
###Memory managmnet:

1.  отличие кучи и стека. Как выделяется память в iOS?
- где выделяется память (блок - выделяется в стек, но можно скопировать в кучу, `NSObjects` - куча)
- хвостовая рекурсия в obj-c (невозможна, подробнее тут: http://devetc.org/code/2014/05/24/tail-recursion-objc-and-arc.html )


###Blocks
1. Расскажи про блоки. Что это, как их использовать. Особенности аллоцирования памяти для блоков.
(Block memory managment specifics):
	- The blocks retain the objects they reference (unless the object is qualified, such as `__weak` in ARC)
	- Non-local variables are copied and stored with the block as const variables
	- Передавать внутрь только слабые ссылки на `self`
	- Хранить блоки в пропети только как `copy`
 	- Что думаешь про этот код?

	 	```
	 	self.block = ^{
	    [self f1];
	    [self f2];
		};
		```

		**Ответ:** retain cycle. Correct:

		```
		__weak __typeof(self)weakSelf = self;
		self.block = ^{
		    __strong typeof(self)strongSelf = weakSelf;
		    [weakSelf m1];
		    [weakSelf m2];
		};
		```


	- Что выведет этот код?

		```
		NSString *s1 = @"foo";
		NSString*(^printString)(NSString*) = ^(NSString *str){
		    NSLog(@"s1 = %@", s1);
		    s1 = @"abc";
		    return str;
		};
		s1 = @"foo2";
		NSString *s3 = printString(s2);
		NSLog(@"s1 = %@", s1);
		NSLog(@"s3 = %@", s3);
	   ```

		**Ответ:**

		- будет ошибка компиляции, т.к. `s1` присваивается значение внутри блока. Что бы это делать - нужно использовать `__block`.

   - Что выведет этот код:

   	```
   __block NSString *s1 = @"foo";
    NSString *s2 = @"bar";
    NSString*(^printString)(NSString*) = ^(NSString *str){
        NSLog(@"s1 = %@", s1);
        NSLog(@"s2 = %@", s2);
        NSLog(@"str = %@", str);
        s1 = @"norf";
        return str;
    };
    s1 = @"qux";
    s2 = @"xyz";
    NSString *s3 = printString(s2);
    NSLog(@"s1 = %@", s1);
    NSLog(@"s2 = %@", s2);
    NSLog(@"s3 = %@", s3);
   ```

	**Ответ:**

	- `s1` передается слабая ссылка (`__block`).
	- `s2` копируется внутрь блока, поэтому его значение не меняется


	```
	s1 = qux
	s2 = bar
	str = xyz
	s1 = norf
	s2 = xyz
	```


1. компиляторы, отличие и прочее.

1. чем отличаются статические библиотеки от динамический и что такое фреймвроки.

1. Критические секции, как с ними работать
- какие виды мьютексов бывают


1. Парадигмы программирования (императивное и декларативное)

1. Инкапсуляция, для чего она?

1. Как реализовать waterfall (цепочка вызовов). Как обрабатывать ошибки?


###Misc:
1. Какое твое кредо, в чем ты силен?
1. Есть ли свои открытые проекты?

---
Книги
Code Complete
Book by Steve McConnell

Growing Object-Oriented Software Guided by Tests
http://www.growing-object-oriented-software.com/

- http://it-interview.ru/iphone-razrabotchik-voprosy-iz-sobesedovanij/
- http://it-interview.ru/sobesedovanie-objective-c-iphone-ios-razrabotchik-v-sankt-peterburge/
- http://it-interview.ru/voprosy-i-otvety-na-sobesedovanii-dlya-ios-razrabotchika/
- http://it-interview.ru/voprosy-i-otvety-na-sobesedovanii-dlya-ios-razrabotchika-2/
